
<?php

function calcularOperacao($num1, $num2, $operador) {
    $soma = 0;

    switch ($operador) {
        case '+':
            $soma = $num1 + $num2;
            break;
        case '-':
            $soma = $num1 - $num2;
            break;
        case '*':
            $soma = $num1 * $num2;
            break;
        case '/':
            if ($num2 != 0) {
                $soma = $num1 / $num2;
            } else {
                $soma = "Erro: divisão por zero!";
            }
            break;
        default:
            $soma = "Operador inválido!";
            break;
    }

    return $soma;
}


function salvarHistorico($num1, $num2, $operador, $resultado) {

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    if (!empty($resultado)) {

        $expressao = $num1 . ' ' . $operador . ' ' . $num2 . ' = ' . $resultado;
        

        $_SESSION['historico'][] = $expressao;
    }
}

function limparHistorico() {

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }


    $_SESSION['historico'] = array();
}

?>

