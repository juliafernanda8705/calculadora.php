<?php
session_start();

// Inclui as funções
include 'funcoes.php';

if (!isset($_SESSION['historico'])) {
    $_SESSION['historico'] = array();
}

if (!isset($_SESSION['num1'])) {
    $_SESSION['num1'] = "";
}
if (!isset($_SESSION['num2'])) {
    $_SESSION['num2'] = "";
}
if (!isset($_SESSION['operador'])) {
    $_SESSION['operador'] = "+";
}
if (!isset($_SESSION['resultado'])) {
    $_SESSION['resultado'] = "";
}

$num1 = $_SESSION['num1'];
$num2 = $_SESSION['num2'];
$operador = $_SESSION['operador'];
$resultado = $_SESSION['resultado'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['calcular'])) {
        $num1 = $_POST['numero1'];
        $num2 = $_POST['numero2'];
        $operador = $_POST['operadores'];

        $resultado = calcularOperacao($num1, $num2, $operador);

        $_SESSION['num1'] = $num1;
        $_SESSION['num2'] = $num2;
        $_SESSION['operador'] = $operador;
        $_SESSION['resultado'] = $resultado; 
    }

    if (isset($_POST['salvar'])) {
        salvarHistorico($num1, $num2, $operador, $resultado);
    }

    if (isset($_POST['apagarHistorico'])) {
        limparHistorico();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
    <link rel="stylesheet" href="style.css">
    <link rel="funcoes" href="funcoes.php">
</head>
<body>
    
    <h1 class="titulo">Calculadora PHP</h1>

    <form action="" method="post">

    <div class="corpinho">    
        <div class="numero1">
            <label for="numero1">Numero 1</label>
            <input type="number" name="numero1" id="" value="<?php echo $num1; ?>">
        </div>

        <select class="operadores" id="operadores" name="operadores">
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
        </select>
        
        <div class="numero2">
            <label for="numero2">Numero 2</label>
            <input type="number" name="numero2" id="" value="<?php echo $num2; ?>">
        </div>

    </div>

    <br></br>

    <div class="calcular">
        <button type="submit" name="calcular">Calcular</button>
    </div>

    <br></br>

    <div class="resul">
        <label for="resultado">Resultado</label>
        <input type="text" name="resultado" id="resultado" value="<?php echo $resultado; ?>" readonly>
    </div>

    <br></br>
    <br></br>
    <br></br>

    <div class="botao">
        <button type="submit" name="salvar">Salvar</button>
        <button type="submit" name="pegarValores">Pegar Valores</button>
        <button type="submit" name="limparMemoria">M</button>
        <button type="submit" name="apagarHistorico">Apagar Histórico</button>
    </div>

    <h1 class="tit">Histórico</h1>

    <div class="historicos">
        <?php
        foreach ($_SESSION['historico'] as $item) {
            echo $item . '<br>';
        }
        ?>
    </div>

    </form>

</body>
</html>